-- Update the rental duration and rental rate of the film
UPDATE film
SET rental_duration = 3, rental_rate = 9.99
WHERE title = 'The Magnificent Seven';

-- Update the personal data of the customer
UPDATE customer
SET first_name = 'Nurillo', last_name = 'Okhunov', email = 'okhunov1819@gmail.com', address_id = (SELECT address_id FROM address ORDER BY RANDOM() LIMIT 1)
WHERE customer_id IN (
    SELECT rental.customer_id
    FROM rental
    JOIN payment ON rental.customer_id = payment.customer_id
    GROUP BY rental.customer_id
    HAVING COUNT(DISTINCT rental.rental_id) >= 10 AND COUNT(DISTINCT payment.payment_id) >= 10
);

-- Update the create_date of the customer
UPDATE customer
SET create_date = CURRENT_DATE
WHERE customer_id IN (
    SELECT rental.customer_id
    FROM rental
    JOIN payment ON rental.customer_id = payment.customer_id
    GROUP BY rental.customer_id
    HAVING COUNT(DISTINCT rental.rental_id) >= 10 AND COUNT(DISTINCT payment.payment_id) >= 10
);